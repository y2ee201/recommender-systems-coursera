package edu.umn.cs.recsys.uu;

import it.unimi.dsi.fastutil.longs.LongSet;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.grouplens.lenskit.basic.AbstractItemScorer;
import org.grouplens.lenskit.data.dao.ItemEventDAO;
import org.grouplens.lenskit.data.dao.UserEventDAO;
import org.grouplens.lenskit.data.event.Rating;
import org.grouplens.lenskit.data.history.History;
import org.grouplens.lenskit.data.history.RatingVectorUserHistorySummarizer;
import org.grouplens.lenskit.data.history.UserHistory;
import org.grouplens.lenskit.vectors.MutableSparseVector;
import org.grouplens.lenskit.vectors.SparseVector;
import org.grouplens.lenskit.vectors.VectorEntry;
import org.grouplens.lenskit.vectors.similarity.CosineVectorSimilarity;

/**
 * User-user item scorer.
 * 
 * @author <a href="http://www.grouplens.org">GroupLens Research</a>
 */
public class SimpleUserUserItemScorer extends AbstractItemScorer {
	private final UserEventDAO userDao;
	private final ItemEventDAO itemDao;

	@Inject
	public SimpleUserUserItemScorer(UserEventDAO udao, ItemEventDAO idao) {
		userDao = udao;
		itemDao = idao;
	}

	@Override
	public void score(long user, @Nonnull MutableSparseVector scores) {
		SparseVector userVector = getUserRatingVector(user);
		double cs = 0;
		// TODO Score items for this user using user-user collaborative
		// filtering

		// User's Mean Centered Item Vector
		MutableSparseVector userVectorMeanCen = userVector.mutableCopy();
		userVectorMeanCen.add(-1 * userVector.mean());
		// System.out.println(userVectorMeanCen);
		/*
		 * Map<Long, Double> tMap = new TreeMap<Long, Double>();
		 * 
		 * for (VectorEntry en : userVectorMeanCen.fast()) { LongSet
		 * possibleNeighbours = itemDao.getUsersForItem(en.getKey());
		 * 
		 * while (possibleNeighbours.iterator().hasNext()) { SparseVector
		 * neighbourVec = getUserRatingVector(possibleNeighbours
		 * .iterator().next()); MutableSparseVector neighbourVecMeanCen =
		 * neighbourVec .mutableCopy(); neighbourVecMeanCen.add(-1 *
		 * neighbourVec.mean()); cs = new
		 * CosineVectorSimilarity().similarity(userVectorMeanCen,
		 * neighbourVecMeanCen); tMap = new TreeMap<Long, Double>();
		 * tMap.put(en.getKey(), cs); }
		 * 
		 * } int i = 1; for (Map.Entry<Long, Double> entry : tMap.entrySet()) {
		 * if (entry.getKey() != user) {
		 * 
		 * i++; } if (i > 30) break; } ;
		 */

		// This is the loop structure to iterate over items to score
		for (VectorEntry e : scores.fast(VectorEntry.State.EITHER)) {
			// Top 30 similar users
			Map<Double, Long> tMap = new TreeMap<Double, Long>();
			LongSet possibleNeighbours = itemDao.getUsersForItem(e.getKey());

			for (Long l : possibleNeighbours.toLongArray()) {
				SparseVector neighbourVec = getUserRatingVector(l);
				MutableSparseVector neighbourVecMeanCen = neighbourVec
						.mutableCopy();
				neighbourVecMeanCen.add(-1 * neighbourVec.mean());
				cs = new CosineVectorSimilarity().similarity(userVectorMeanCen,
						neighbourVecMeanCen);
				tMap.put(cs, l);

			}
			System.out.println(tMap);
			/*int i = 1;
			for (Map.Entry<Long, Double> entry : tMap.entrySet()) {
				if (entry.getKey() != user) {
					
					i++;
				}
				if (i > 30)
					break;
			}
			;*/

		}
	}

	/**
	 * Get a user's rating vector.
	 * 
	 * @param user
	 *            The user ID.
	 * @return The rating vector.
	 */
	private SparseVector getUserRatingVector(long user) {
		UserHistory<Rating> history = userDao.getEventsForUser(user,
				Rating.class);
		if (history == null) {
			history = History.forUser(user);
		}
		return RatingVectorUserHistorySummarizer.makeRatingVector(history);
	}

}
